export EOS_DEVICE=lilac
export EOS_CCACHE_SIZE=200GB
export EOS_SIGNATURE_SPOOFING=restricted
export EOS_CCACHE_DIR=/home/pete/srv/custom/cache
export ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx8G"
export BRANCH_NAME="v0.22-q"

########### extendrom section ###########
$PWD/vendor/extendrom/get_prebuilts.sh


export ENABLE_EXTENDROM=true

########### "traditional" custom build ###########

#export EOS_RELEASE_TYPE=CUSTOM
#export EXTENDROM_PACKAGES="noEOSappstore Omega F-Droid F-DroidPrivilegedExtension_pb AuroraStore additional_repos.xml Lawnchair-stable K9-Mail-latest Fennec QKSMS Etar noEOSMail noEOSCalendar noEOSMessage noEOSPdfViewer noEOSLibreOfficeViewer noEOSCamera MicrogGmsCore AuroraServices FFUpdater"

########### UPSTREAM custom build ###########
export EOS_RELEASE_TYPE=CUSTOM-UPSTREAM
export EXTENDROM_PACKAGES="noEOSlauncher noEOSBlissIconPack noEOSAccountManager noEOSeDrive noEOSNotes noEOSESmsSync noEOSappstore noEOSMessage noEOSPdfViewer noEOSLibreOfficeViewer noEOSCamera noEOSeSpeakTTS noEOSMail noEOSCalendar  noEOSTasks noEOSBrowser noEOSOpenKeychain noEOSOpenWeatherMapWeatherProvider F-Droid F-DroidPrivilegedExtension_pb  AuroraStore AuroraServices ICSx5 DAVx5 NextCloud K9-Mail-latest Fennec QKSMS Etar OpenTasks MicrogGmsCore  FFUpdater OpenCamera NextCloudNotes"

